﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using static PurpleVM.VM.OpCode;
using System.Windows.Input;

namespace PurpleVM
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {

            VMBytecodeGenerator a = new VMBytecodeGenerator();
            //TODO: Make demo pong game
            const int PADDLE_HEIGHT = 2;
            const int LBL_DRAW_FIELD = 0;
            const int LBL_DRAW_PADDLE = 1;
            const int LBL_UPDATE_BALL = 2;
            const int LBL_DRAW_BALL = 3;

            const byte RACKET_1_X = 11;
            const byte RACKET_2_X = 11 + 38;

            const byte RACKET_1_Y = 10;
            const byte RACKET_2_Y = 11;
            const byte BALL_POS_X = 12;
            const byte BALL_POS_Y = 13;
            const byte BALL_VEL_X = 14;
            const byte BALL_VEL_Y = 15;
            //MEMORY LOCATIONS

            //10 - racket 1 Y
            //11 - racket 2 Y
            //12 - ball posX
            //13 - ball posY
            //14 - ball velX
            //15 - ball velY
           
            a.LOOP_START();

            a.CALL(LBL_DRAW_FIELD);

            a.PUSH(0);
            a.CALL(LBL_DRAW_PADDLE);
            a.PUSH(1);
            a.CALL(LBL_DRAW_PADDLE);

            a.LOOP_GOTO();
            //LBL_DRAW_FIELD
            {
                a.LABEL(LBL_DRAW_FIELD);
                a.SET_MEM_EX(0, 0);

                a.SET_MEM_EX(2, 24);
                a.LOOP_START();
                a.PLOTMEM_PIX(0, 1);
                a.PLOTMEM_PIX(0, 2);
                a.INC_MEM(0, 1);

                a.PUSH(60);
                a.GET_MEM(0);
                a.NOT_EQUAL();
                a.LOOP_GOTO();
                a.LOOP_FREE();
                a.RET();
            }
            //LBL_DRAW_PADDLE
            {
                a.LABEL(LBL_DRAW_PADDLE);
                a.SET_MEM(0);

                a.RET();
            }

            a.WriteProgramToFile("input");
            // return;
            VM.Reset();
            VM.ExtFunctions.Add(0, VMExternalFunctions.PrintText);
            VM.ExtFunctions.Add(1, VMExternalFunctions.Wait);
            VM.ExtFunctions.Add(2, VMExternalFunctions.SetCursorPos);
            VM.ExtFunctions.Add(3, VMExternalFunctions.DrawPixel);
            VM.ExtFunctions.Add(4, VMExternalFunctions.GetKey);
            VM.ExtFunctions.Add(5, VMExternalFunctions.KeyPressed);
            VM.ExtFunctions.Add(6, VMExternalFunctions.ClearScreen);
            VM.ExecuteProgram(a);
            VM.ShowState();
            Console.ReadKey();
            //
        }
        /*a.LOOP_START();

            a.KEYDOWN('W',0);
            a.CALL(0);

            a.KEYDOWN('S',1);
            a.LOOP_GOTO();

            a.DONE();

            a.LABEL(0);
            a.PUSH((byte)'H');
            a.CALL_FUNC((byte)VM.ExternalInstructions.PrintText);
            a.WAIT(25);
            a.RET();*/


        static void Trail(VMBytecodeGenerator a)
        {
            a.LOOP_START();

            a.KEYDOWN('W', 0); a.CALL(0);
            a.KEYDOWN('S', 0); a.CALL(1);
            a.KEYDOWN('A', 0); a.CALL(2);
            a.KEYDOWN('D', 0); a.CALL(3);

            a.INC_MEM(2, 1); a.PLOTMEM_PIX(0, 1);
            a.WAIT(25);
            a.LOOP_GOTO();

            //W
            a.LABEL(0);
            a.DEC_MEM(1, 1);
            a.RET();
            //S
            a.LABEL(1);
            a.INC_MEM(1, 1);
            a.RET();
            //A
            a.LABEL(2);
            a.DEC_MEM(0, 1);
            a.RET();
            //D
            a.LABEL(3);
            a.INC_MEM(0, 1);
            a.RET();
        }
        static void X(VMBytecodeGenerator a)
        {
            a.SET_MEM_EX(0, 0);
            a.SET_MEM_EX(1, 26);
            a.LOOP_START();
            a.PLOTMEM_PIX(0, 0);
            a.GET_MEM(0);
            a.GET_MEM(1);
            a.SUB();


            a.SET_MEM(5);

            a.PLOTMEM_PIX(5, 0);
            a.INC_MEM(0, 1);

            a.CMPMEM_LESS_OR_EQ(0, 1);
            a.LOOP_GOTO();

            a.LABEL(0);
            a.GOTO(0);


            a.DONE();
        }
    }
}
