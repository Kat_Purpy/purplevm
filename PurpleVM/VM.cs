﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PurpleVM
{
    public static class VM
    {
        public const string Prefix = "KAT";
        public static readonly byte[] PrefixBytes = new[] { (byte)'K', (byte)'A', (byte)'T' };
        const int MAX_MEMORY = byte.MaxValue + 1;
        public static int InstructionPointer;
        public static byte ExternalInstructionID;
        public static byte ExternalInstructionResult;

        public static byte[] Program;
        public static byte[] Memory;


        public static Stack<int> ReturnAddress;
        public static Stack<byte> Stack;

        public static Dictionary<byte, ushort> IAT;

        public delegate byte ExternalInstructionDelegate(byte[] args);
        public static Dictionary<byte, ExternalInstructionDelegate> ExtFunctions;

        public enum ExternalInstructions : byte
        {
            PrintText = 0,
            Wait = 1,
            SetCursorPos = 2,
            DrawPixel = 3,
            GetKey = 4,
            KeyPressed = 5,
            ClearScreen = 6
        }

        public enum OpCode : byte
        {
            //Stack
            OP_PUSH = 0,
            OP_POP = 1,

            //Math
            OP_ADD = 2,
            OP_SUB = 3,
            OP_DIV = 4,
            OP_MUL = 5,
            OP_MODULO = 6,

            OP_JUMP = 7,


            //Conditional (if true execute next bytecode)
            OP_EQUAL = 8,
            OP_BIGGER = 9,
            OP_LESS = 10,
            OP_BIGGER_OR_EQ = 11,
            OP_LESS_OR_EQ = 12,
            OP_NOT_EQUAL = 13,

            //External functions
            OP_SET_FUNC = 14,
            OP_CALL_FUNC =15,
            OP_GET_FUNC = 16,
            
            //Memory
            OP_SET_MEM = 17,
            OP_GET_MEM = 18,

            //Loops
            OP_PUSH_RET = 19,
            OP_PEEK_RET = 20,
            OP_FREE_RET = 21,
            
            OP_CALL = 22,
            OP_RET = 23,

            OP_DONE = 0xff,
        }

        public static void ShowState()
        {
            Console.WriteLine("VM PROGRAM");
            for(int i = 0; i < Program.Length; i++)
            {
                if (i > 0 && i % 16 == 0) Console.Write('\n');
                Console.Write(i == InstructionPointer ? "[{0:X2}] " : "{0:X2} ", Program[i]);
            }
            Console.WriteLine("\n\nVM IAT");
            foreach (KeyValuePair<byte, ushort> kvp in IAT) {
                Console.WriteLine("{0} {1:0}",kvp.Key,kvp.Value);
            }

            Console.Write("\n");
            Console.WriteLine("VM MEMORY");
            for(int i = 0; i < Memory.Length; i++)
            {
                if (i > 0 && i % 16 == 0) Console.Write('\n');
                Console.Write("{0:X2} ", Memory[i]);
            }
            Console.Write("\nReturn address: ");
            for(int i = 0; i < ReturnAddress.Count; i++)
            {
                Console.Write(ReturnAddress.ToArray()[i]);
                if(i < ReturnAddress.Count-1) Console.Write(',');

            }
            Console.Write("\n\n");
            Console.Write("VM STACK\n");
            for(int i = 0; i < Stack.Count; i++)
            {
                Console.WriteLine(Stack.ToArray()[i]);
            }
            
        }

        public static void Reset()
        {
            Memory = new byte[MAX_MEMORY];
            Stack = new Stack<byte>();
            ReturnAddress = new Stack<int>();
            IAT = new Dictionary<byte, ushort>();
            ExtFunctions = new Dictionary<byte, ExternalInstructionDelegate>();
            InstructionPointer = 0;
        }

        public static void Message(string msg)
        {
            Message("{0}",msg);
        }
        public static void Error(string msg)
        {
            Message("[ERROR] {0}",msg);
        }
        public static void Message(string format,params object[] obj)
        {
            Console.WriteLine("[VM] " + format,obj);
        }

        public static void ExecuteProgram(VMBytecodeGenerator gen)
        {
            LoadIAT(gen.IAT);
            try {
                InterpretCode(gen.GenerateByteCode());
            }
            catch(DivideByZeroException)
            {
                Error("Tried to divide by zero!");
            }
            
        }
        public static void LoadIAT(Dictionary<byte,ushort> _IAT)
        {
            IAT = _IAT;
        }
        public static void InterpretCode(byte[] opcodes)
        {
            Program = opcodes;
            for (; ; )
            {
                
                switch ((OpCode)opcodes[InstructionPointer])
                {
                    case OpCode.OP_PUSH:
                        Stack.Push(opcodes[++InstructionPointer]);
                        break;
                    case OpCode.OP_POP:
                        Stack.Pop();
                        break;
                    case OpCode.OP_ADD:
                        Stack.Push((byte)(Stack.Pop() + Stack.Pop()));
                        break;
                    case OpCode.OP_SUB:
                        Stack.Push((byte)((Stack.Pop() - Stack.Pop()) % byte.MaxValue));
                        break;
                    case OpCode.OP_DIV:
                        Stack.Push((byte)(Stack.Pop() / Stack.Pop()));
                        break;
                    case OpCode.OP_MUL:
                        Stack.Push((byte)((Stack.Pop() * Stack.Pop()) % byte.MaxValue));
                        break;
                    case OpCode.OP_MODULO:
                        Stack.Push((byte)((Stack.Pop() % Stack.Pop()) % byte.MaxValue));
                        break;
                    case OpCode.OP_JUMP:
                        InstructionPointer = IAT[(byte)(Stack.Pop())]-1;
                        break;
                    case OpCode.OP_BIGGER:
                        if (Stack.Pop() > Stack.Pop()) {}
                        SkipInstructions();
                        break;
                    case OpCode.OP_BIGGER_OR_EQ:
                        if (Stack.Pop() >= Stack.Pop()) { }
                        else SkipInstructions();
                        break;
                    case OpCode.OP_LESS:
                        if (Stack.Pop() < Stack.Pop()) {}
                        else SkipInstructions();
                        break;
                    case OpCode.OP_LESS_OR_EQ:
                        if (Stack.Pop() <= Stack.Pop()) { }
                        else SkipInstructions();
                        break;
                    case OpCode.OP_EQUAL:
                        if (Stack.Pop() == Stack.Pop()) {}
                        else SkipInstructions();
                        break;
                    case OpCode.OP_NOT_EQUAL:
                        if (Stack.Pop() != Stack.Pop()) { }
                        else SkipInstructions();
                        break;
                    case OpCode.OP_SET_FUNC:
                        ExternalInstructionID = Stack.Pop();
                        break;
                    case OpCode.OP_CALL_FUNC:
                        List<byte> args = new List<byte>();
                        while (Stack.Count > 0) args.Add(Stack.Pop());
                        args.Reverse();
                        ExternalInstructionResult = ExtFunctions[ExternalInstructionID].Invoke(args.ToArray());
                        break;
                    case OpCode.OP_GET_FUNC:
                        Stack.Push(ExternalInstructionResult);
                        break;
                    case OpCode.OP_SET_MEM:
                        byte index = Stack.Pop();
                        byte value = Stack.Pop();
                        
                        Memory[index] = value;
                        break;
                    case OpCode.OP_GET_MEM:
                        Stack.Push(Memory[Stack.Pop()]);
                        break;
                    case OpCode.OP_PUSH_RET:
                        ReturnAddress.Push(InstructionPointer);
                        break;
                    case OpCode.OP_PEEK_RET:
                        InstructionPointer = ReturnAddress.Peek();
                        break;
                    case OpCode.OP_FREE_RET:
                        ReturnAddress.Pop();
                        break;
                    case OpCode.OP_CALL:
                        ReturnAddress.Push(InstructionPointer);
                        InstructionPointer = IAT[(byte)(Stack.Pop())]-1;
                        break;
                    case OpCode.OP_RET:
                        if (ReturnAddress.Count > 0)
                        {
                            InstructionPointer = ReturnAddress.Pop();
                            break;
                        }
                        break;
                    case OpCode.OP_DONE:
                        // Message("Done");
                        return;
                    default:
                        ShowState();
                        Message("Invalid opcode {0} at {1}", opcodes[InstructionPointer],InstructionPointer);
                        Console.ReadKey();
                        return;
                }

                InstructionPointer++;
            }
        }
        static void SkipInstructions()
        {
            //if(Program[InstructionPointer+2] == (byte)OpCode.OP_JUMP)
            // {
            //     InstructionPointer += 2;
            //       while (true) { }
            //         return;
            //  }
            if(Program[InstructionPointer+3] == (byte)OpCode.OP_JUMP ||
               Program[InstructionPointer + 3] == (byte)OpCode.OP_CALL)
            {
                InstructionPointer++;
                InstructionPointer++;
            }
            InstructionPointer++;
         ///   ShowState();
          //  Console.ReadKey();
        }

    }
    public class Extensions
    {
       
    }
}
