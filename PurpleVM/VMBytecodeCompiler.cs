﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
namespace PurpleVM
{
    public static class VMBytecodeCompiler
    {
        public static VMBytecodeGenerator generator;
        static readonly Type generatorType = typeof(VMBytecodeGenerator);
        static readonly MethodInfo[] methodInfos = typeof(VMBytecodeGenerator).GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);
        public static VMBytecodeGenerator CompileCodeFromFile(string path)
        {
            return CompileCode(System.IO.File.ReadAllText(path));
        }
        public static VMBytecodeGenerator CompileCode(string code)
        {
            string[] instructions = code.Split(new[] { '\n' }, StringSplitOptions.RemoveEmptyEntries)
                .Where((s) =>
                {
                    s = RemoveSpecialCharacters(s);
                    return !(string.IsNullOrEmpty(s) && string.IsNullOrWhiteSpace(s));
                })
                .ToArray();

            generator = new VMBytecodeGenerator();
            for (int i = 0; i < instructions.Length; i++)
            {
                ParseCommand(instructions[i]);
            }

            return generator;
        }
        static void ParseCommand(string instruction)
        {
            List<string> ops = new List<string>(instruction.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries));
            string operation = string.Copy(ops[0]);
            ops.RemoveAt(0);
            string[] arguments = ops.ToArray();

            MethodBase method = generatorType.GetMethod(RemoveSpecialCharacters(operation));
            bool RequireArguments = method.GetParameters() != null;
            if (RequireArguments)
            {
                method.Invoke(generator, arguments
                    .Select((b) =>
                        {
                            if (byte.TryParse(b, out byte @byte))
                            {
                                return (object)@byte;
                            }
                            if ("abcdefghijklmnopqrstuvwxyz".ToList().Contains(b.ToLower()[0]))
                            {
                                return Encoding.ASCII.GetBytes(new char[] { b[0] })[0];
                            }
                            return null;
                        })
                    .ToArray());
            } else {
                method.Invoke(generator, null);
            }
        }
        public static string RemoveSpecialCharacters(this string str)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in str)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '.' || c == '_')
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }
    }
}
