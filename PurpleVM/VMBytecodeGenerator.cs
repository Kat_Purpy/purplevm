﻿using System;
using System.Collections.Generic;
using System.IO;
using static PurpleVM.VM.OpCode;
using static PurpleVM.VM.ExternalInstructions;
namespace PurpleVM
{
    public class VMBytecodeGenerator
    {
        List<byte> bytes = new List<byte>();
        public Dictionary<byte, ushort> IAT = new Dictionary<byte, ushort>();
        public byte[] GenerateByteCode()
        {
            return bytes.ToArray();
        }
        public void WriteProgramToFile(string file)
        {
            FileStream fs = new FileStream(file, FileMode.Create);
            BinaryWriter sw = new BinaryWriter(fs,System.Text.Encoding.ASCII);
            sw.Write((byte)'K');
            sw.Write((byte)'A');
            sw.Write((byte)'T');
            sw.Write(IAT.Count);
            foreach (KeyValuePair<byte,ushort> kvp in IAT)
            {
                sw.Write(kvp.Key);
                sw.Write(kvp.Value);
            }
            sw.Write(bytes.ToArray());
            sw.Dispose();
            fs.Dispose();
        }
        public void LoadProgramFromFile(string path)
        {
            FileStream fs = new FileStream(path, FileMode.Open);
            BinaryReader br = new BinaryReader(fs);
            byte[] prefix = br.ReadBytes(3); 
            if(!(prefix[0] == VM.PrefixBytes[0] &&
                prefix[1] == VM.PrefixBytes[1] &&
                    prefix[2] == VM.PrefixBytes[2])) throw new Exception("This file is not a VM program.");

            int IATLength = br.ReadInt32();
            for(int i = 0; i < IATLength; i++)
            {
                IAT.Add(br.ReadByte(), br.ReadUInt16());
            }
            bytes = new List<byte>(br.ReadBytes((int)br.BaseStream.Length));
            br.Dispose();
            fs.Dispose();
        }
        public void DONE()
        {
            bytes.Add((byte)OP_DONE);
        }
        public void PUSH(byte value)
        {
            bytes.Add((byte)OP_PUSH);
            bytes.Add(value);
        }
        public void POP()
        {
            bytes.Add((byte)OP_POP);
        }
        public void ADD()
        {
            bytes.Add((byte)OP_ADD);
        }
        public void SUB()
        {
            bytes.Add((byte)OP_SUB);
        }
        public void DIV()
        {
            bytes.Add((byte)OP_DIV);
        }
        public void MUL()
        {
            bytes.Add((byte)OP_MUL);
        }
        public void MODULO()
        {
            bytes.Add((byte)OP_MODULO);
        }
        public void JUMP()
        {
            bytes.Add((byte)OP_JUMP);
        }
        public void EQUAL()
        {
            bytes.Add((byte)OP_EQUAL);
        }
        public void NOT_EQUAL()
        {
            bytes.Add((byte)OP_NOT_EQUAL);
        }
        public void BIGGER()
        {
            bytes.Add((byte)OP_BIGGER);
        }
        public void BIGGER_OR_EQ()
        {
            bytes.Add((byte)OP_BIGGER_OR_EQ);
        }
        public void LESS()
        {
            bytes.Add((byte)OP_LESS);
        }
        public void LESS_OR_EQ()
        {
            bytes.Add((byte)OP_LESS_OR_EQ);
        }
        public void SET_MEM(byte ID)
        {
            PUSH(ID);
            bytes.Add((byte)OP_SET_MEM);
        }
        
        public void GET_MEM(byte ID)
        {
            PUSH(ID);
            bytes.Add((byte)OP_GET_MEM);
        }
        public void PUSH_RET()
        {
            bytes.Add((byte)OP_PUSH_RET);
        }
        public void GOTO_RET()
        {
            bytes.Add((byte)OP_PEEK_RET);
        }
        public void POP_RET()
        {
            bytes.Add((byte)OP_FREE_RET);
        }
        public void CALL_FUNC(byte func)
        {
            PUSH(func);
            bytes.Add((byte)OP_SET_FUNC);
            bytes.Add((byte)OP_CALL_FUNC);
        }
        public void GET_FUNC()
        {
            bytes.Add((byte)OP_GET_FUNC);
        }
        public void GOTO(byte i)
        {
            PUSH(i);
            bytes.Add((byte)OP_JUMP);
        }
        public void CALL(byte i)
        {
            PUSH(i);
            bytes.Add((byte)OP_CALL);
        }
        public void LABEL(byte i)
        {
            IAT.Add(i,(ushort)bytes.Count);
        }
        public void RET()
        {
            bytes.Add((byte)OP_RET);
        }
        //Sugar zone//
        public void WAIT(byte time)
        {
            PUSH(time);
            CALL_FUNC((byte)Wait);
        }
        public void KEYDOWN(char c, byte invert)
        {
            PUSH((byte)c);
            CALL_FUNC((byte)KeyPressed);
            GET_FUNC();
            PUSH(0);
            if (invert == 1)
                EQUAL();
            else
                NOT_EQUAL();
        }
        public void PLOT_PIX(byte x, byte y)
        {
            PUSH(x);
            PUSH(y);
            CALL_FUNC((byte)DrawPixel);
        }
        public void PLOTMEM_PIX(byte x,byte y)
        {
            GET_MEM(x);
            GET_MEM(y);
            CALL_FUNC((byte)DrawPixel);
        }
        public void LOOP_START()
        {
            PUSH_RET();
        }
        public void LOOP_GOTO()
        {
            GOTO_RET();
        }
        public void LOOP_FREE()
        {
            POP_RET();
        }
        public void INC_MEM(byte ID,byte value)
        {
            GET_MEM(ID);
            PUSH(value);
            ADD();
            SET_MEM(ID);
        }
        public void DEC_MEM(byte ID, byte value)
        {
            PUSH(value);
            GET_MEM(ID);
            SUB();
            SET_MEM(ID);
        }
        public void SET_MEM_EX(byte ID, byte VALUE)
        {
            PUSH(VALUE);
            SET_MEM(ID);
        }
        public void CMPMEM_BIGGER(byte LEFT,byte RIGHT)
        {
            GET_MEM(RIGHT);
            GET_MEM(LEFT);
            
            BIGGER();
        }
        public void CMPMEM_BIGGER_OR_EQ(byte LEFT, byte RIGHT)
        {
            GET_MEM(RIGHT);
            GET_MEM(LEFT);

            BIGGER_OR_EQ();
        }
        public void CMPMEM_LESS(byte LEFT, byte RIGHT)
        {
            GET_MEM(RIGHT);
            GET_MEM(LEFT);
            
            LESS();
        }
        public void CMPMEM_LESS_OR_EQ(byte LEFT, byte RIGHT)
        {
            GET_MEM(RIGHT);
            GET_MEM(LEFT);

            LESS_OR_EQ();
        }
        public void CMPMEM_EQUAL(byte LEFT, byte RIGHT)
        {
            GET_MEM(RIGHT);
            GET_MEM(LEFT);
            
            EQUAL();
        }

    }
}
