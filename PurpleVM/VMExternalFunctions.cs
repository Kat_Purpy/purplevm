﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Input;
namespace PurpleVM
{
    public static class VMExternalFunctions
    {
        public static byte PrintText(byte[] args)
        {
            Console.OpenStandardOutput().Write(args, 0, args.Length);
            return 0;
        }
        public static byte Wait(byte[] args)
        {
            Thread.Sleep(args[0]);
            return 0;
        }
        public static byte SetCursorPos(byte[] args)
        {
            Console.SetCursorPosition(args[0], args[1]);
            return 0;
        }
        public static byte DrawPixel(byte[] args)
        {
            SetCursorPos(args);
            Console.Write('█');
            return 0;
        }
        public static byte GetKey(byte[] args)
        {
            return (byte)Console.ReadKey().KeyChar;
        }
        public static Dictionary<byte,Key> KeyMap = new Dictionary<byte, Key>()
        {
            {(byte)'0',Key.D0 },
            {(byte)'1',Key.D1 },
            {(byte)'2',Key.D2 },
            {(byte)'3',Key.D3 },
            {(byte)'4',Key.D4 },
            {(byte)'5',Key.D5 },
            {(byte)'6',Key.D6 },
            {(byte)'7',Key.D7 },
            {(byte)'8',Key.D8 },
            {(byte)'9',Key.D9 },
            {(byte)'A',Key.A },
            {(byte)'B',Key.B },
            {(byte)'C',Key.C },
            {(byte)'D',Key.D },
            {(byte)'E',Key.E },
            {(byte)'F',Key.F },
            {(byte)'G',Key.G },
            {(byte)'H',Key.H },
            {(byte)'I',Key.I },
            {(byte)'J',Key.J },
            {(byte)'K',Key.K },
            {(byte)'L',Key.L },
            {(byte)'M',Key.M },
            {(byte)'N',Key.N },
            {(byte)'O',Key.O },
            {(byte)'P',Key.P },
            {(byte)'Q',Key.Q },
            {(byte)'R',Key.R },
            {(byte)'S',Key.S },
            {(byte)'T',Key.T },
            {(byte)'U',Key.U },
            {(byte)'V',Key.V },
            {(byte)'W',Key.W },
            {(byte)'X',Key.X },
            {(byte)'Y',Key.Y },
            {(byte)'Z',Key.Z },
        };
        public static byte KeyPressed(byte[] args)
        {
            if (!KeyMap.ContainsKey(args[0]))
            {
                VM.Message("Key {0} does not exist",args[0]);
                while (true) { }
            }
            return (byte)(Keyboard.IsKeyDown(KeyMap[args[0]]) ? 1 : 0);
        }
        public static byte ClearScreen(byte[] args)
        {
            Console.Clear();
            return 0;
        }
    }
}
