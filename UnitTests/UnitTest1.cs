﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PurpleVM;
namespace UnitTests
{
    [TestClass]
    public class VMLogicTests
    {
        [TestMethod]
        public void BasicMathAndMemoryAccessTest()
        {
            VMBytecodeGenerator gen = new VMBytecodeGenerator();

            //(10 + 5 * 5) * 2 = 70
            gen.PUSH(5);
            gen.PUSH(5);
            gen.MUL();
            gen.PUSH(10);
            gen.ADD();
            gen.PUSH(2);
            gen.MUL();
            gen.SET_MEM(0);

            gen.DONE();

            VM.Reset();
            
            VM.InterpretCode(gen.GenerateByteCode());
            Assert.AreEqual(VM.Memory[0], 70);
        }

        [TestMethod]
        public void LoopsTest()
        {
            const byte LoopIterations = 8;
            VM.Reset();

            VMBytecodeGenerator a = new VMBytecodeGenerator();
            a.SET_MEM_EX(1, 8);
            a.LOOP_START();
            a.INC_MEM(0, 1);
            a.CMPMEM_LESS(0,1);
            a.LOOP_GOTO();

            a.LOOP_FREE();
            a.DONE();

            VM.ExecuteProgram(a);
            Assert.AreEqual(LoopIterations, VM.Memory[0]);
        }

        [TestMethod]
        public void ExternalFunctionsTest()
        {
            string s = "";

            VMBytecodeGenerator gen = new VMBytecodeGenerator();

            gen.PUSH((byte)'H'); 
            gen.PUSH((byte)'e');
            gen.PUSH((byte)'l');
            gen.PUSH((byte)'l');
            gen.PUSH((byte)'o');
            gen.PUSH((byte)' ');
            gen.PUSH((byte)'w');
            gen.PUSH((byte)'o');
            gen.PUSH((byte)'r');
            gen.PUSH((byte)'l');
            gen.PUSH((byte)'d');

            gen.CALL_FUNC(0);
            gen.DONE();
            VM.Reset();
            VM.ExtFunctions.Add(0, (b) => {
                foreach (byte a in b)
                {
                    s += (char)a;
                }
                return 0;
            });
            VM.ExecuteProgram(gen);
            Assert.AreEqual("Hello world", s);
        }
        [TestMethod]
        public void GotoTest()
        {
            VMBytecodeGenerator a = new VMBytecodeGenerator();
            a.GOTO(0);
            a.DONE();

            a.LABEL(0);
            a.SET_MEM_EX(0, 0xca);
            a.SET_MEM_EX(1, 0xfe);
            a.SET_MEM_EX(2, 0xba);
            a.SET_MEM_EX(3, 0xbe);

            a.DONE();
            VM.Reset();
            VM.ExecuteProgram(a);
            Assert.IsTrue(
                VM.Memory[0] == 0xca &&
                VM.Memory[1] == 0xfe &&
                VM.Memory[2] == 0xba &&
                VM.Memory[3] == 0xbe
                );
        }
    }
}
