#pragma once
typedef enum{
	//Stack
	OP_PUSH = 0,
	OP_POP,
	
	//Math
	OP_ADD,
	OP_SUB,
	OP_DIV,
	OP_MUL,
	OP_MODULO,
	
	OP_JUMP,
	
	//Conditional
	OP_EQUAL,
	OP_BIGGER,
	OP_LESS,
	OP_CONDJMP,
	
	//External functions
	OP_SET_FUNC,
	OP_CALL_FUNC,
	OP_GET_FUNC,
	
	OP_DONE = 0xff,
} opcode;
#define PROGRAM_MAX 512
#define STACK_MAX 256
#define MAX_EXTERNAL_FUNCS 256
struct vm{
	char program[PROGRAM_MAX];
	char stack[STACK_MAX];
	int* stack_top;
	int ip; //instruction pointer
	int (*external_funcs)(char[] args)[MAX_INSTRUCTIONS];
} vm;

void vm_reset(){
	vm.stack_top = vm.stack;
	vm.ip = 0;
}

void vmstack_push(int val){
	*vm.stack_top = val;
	vm.stack_top++;
}
int vmstack_pop(void){
	vm.stack_top--;
	return *vm.stack_top;
}
void vm_execute(char[] commands){
	
}
int vm_interpret(char opcode){
	switch(command){
		case OP_PUSH:
		vmstack_push(vm.ip++);
		break;
		case OP_POP:
		break;
	}
}